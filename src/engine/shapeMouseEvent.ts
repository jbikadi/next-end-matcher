import { MouseEvent as ReactMouseEvent } from "react";

export function handleMouseDown(e: ReactMouseEvent<HTMLButtonElement>) {
    const board = document.querySelector('div.board') as HTMLDivElement;
    const target = e.target as HTMLButtonElement;
    const shiftX = e.clientX - target.getBoundingClientRect().left;
    const shiftY = e.clientY - target.getBoundingClientRect().top;
    const placeHolder = document.createElement('div');
    const currentIndex = Array.prototype.indexOf.call(target.parentNode?.children, target);
    const parentIndex = Array.prototype.indexOf.call(board.children, target.parentNode);

    placeHolder.classList.add('placeholder', 'w-8', 'h-8');
    moveAt(e.pageX, e.pageY);
    target.classList.add('absolute');
    target.after(placeHolder);

    function moveAt(pageX: number, pageY: number) {
        target.style.left = pageX - shiftX - board.offsetLeft + 'px';
        target.style.top = pageY - shiftY - board.offsetTop + 'px';
    }

    function dragHorizantal(belowParentIndex: number, x: number, elementBelow: Element) {
        const currentCol = board.children[belowParentIndex].children;
        const prevCol = board.children[belowParentIndex + x].children;

        if (currentIndex === 0) {
            // first child
            currentCol[currentIndex].before(placeHolder);
            prevCol[currentIndex].before(elementBelow);
        }  else {
            // middle children
            currentCol[currentIndex].before(placeHolder);
            prevCol[currentIndex - 1].after(currentCol[currentIndex + 1]);
        }
    }

    function handleMouseMove(e: MouseEvent) {
        moveAt(e.pageX, e.pageY);

        target.hidden = true;
        const elementBelow = document.elementFromPoint(e.clientX, e.clientY);
        target.hidden = false;

        if (!elementBelow || !elementBelow.classList.contains('shape')) return;

        const placeHolderY = placeHolder.offsetTop;
        const placeHolderIndex = Array.prototype.indexOf.call(board.children, placeHolder.parentNode);
        const belowParentIndex = Array.prototype.indexOf.call(board.children, elementBelow.parentNode);
        const belowIndex = Array.prototype.indexOf.call(elementBelow.parentNode?.children, elementBelow);

        // drag shape left
        if (belowIndex === currentIndex && placeHolderIndex > belowParentIndex) {
            console.log('left');
            dragHorizantal(belowParentIndex, 1, elementBelow);
        // drag shape right
        } else if (belowIndex === currentIndex && placeHolderIndex < belowParentIndex) {
            console.log('right');
            dragHorizantal(belowParentIndex, -1, elementBelow);
        // drag shape up
        } else if (parentIndex === belowParentIndex && placeHolderY > e.pageY) {
            elementBelow.before(placeHolder);
        // drag shape down
        } else if (parentIndex === belowParentIndex && placeHolderY < e.pageY) {
            elementBelow.after(placeHolder);
        }
    }

    // reset board to previous state with no changes
    function resetBoard() {
        let placeHolderIndex = Array.prototype.indexOf.call(board.children, placeHolder.parentNode);
        while (placeHolderIndex >= 0 && placeHolderIndex !== parentIndex) {
            if (placeHolderIndex > parentIndex) {
                let elementReturn = board.children[placeHolderIndex - 1].children[currentIndex];
                placeHolderIndex -= 1;
                dragHorizantal(placeHolderIndex, 1, elementReturn);
            } else {
                let elementReturn = board.children[placeHolderIndex + 1].children[currentIndex];
                placeHolderIndex += 1;
                dragHorizantal(placeHolderIndex, -1, elementReturn);
            }
        }
        target.classList.remove('absolute');
        target.style.removeProperty('left');
        target.style.removeProperty('top');
        placeHolder.remove();
        board.removeEventListener('mousemove', handleMouseMove);
    }

    board.addEventListener('mousemove', handleMouseMove);

    target.onmouseup = () => {
        resetBoard();
        target.onmouseup = null;
    }

    board.onmouseleave = () => {
        resetBoard();
        board.onmouseleave = null;
    }
}