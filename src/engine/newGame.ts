
function random(difficulty: number) {
    return Math.floor(Math.random() * difficulty);
}

export function newGameBoard(cols: number, rows: number, difficulty: number) {
    return new Array(cols).fill(1).map(() => new Array(rows).fill(difficulty).map(random));
}