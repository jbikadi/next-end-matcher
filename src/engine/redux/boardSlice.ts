import { createSlice, type PayloadAction } from '@reduxjs/toolkit';
import { newGameBoard } from '../newGame';

const initialState: number[][] = [];

export const boardSlice = createSlice({
    name: 'game',
    initialState: {
        board: initialState
    },
    reducers: {
        NEWGAME: (state) => {
            state.board = newGameBoard(6, 5, 5)
        }
    }
});

export const {NEWGAME} = boardSlice.actions;

export default boardSlice.reducer;