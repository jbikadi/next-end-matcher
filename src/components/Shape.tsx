import { shapes } from "@/context/difficulty";
import { handleMouseDown } from "@/engine/shapeMouseEvent";

export function Shape({shapeIndex}: {shapeIndex: number}) {
    const shape = shapes[shapeIndex];

    return (
        <button
            className={shape + ' shape'}
            onMouseDown={(e) => handleMouseDown(e)}
            onDragStart={() => false}
        >
            <span className="sr-only">{shape}</span>
        </button>
    )
}