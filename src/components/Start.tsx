'use client';

import { NEWGAME } from "@/engine/redux/boardSlice";
import { useAppDispatch } from "@/engine/redux/hooks";

export function Start() {
    const dispatch = useAppDispatch();

    return (
    <button
        className="border px-4 py-2 rounded self-center mx-auto hover:bg-slate-700"
        onClick={() => dispatch(NEWGAME())}
    >
        New Game
    </button>
    )
}