'use client';

import { useAppSelector } from "@/engine/redux/hooks";
import { Shape } from "./Shape";
import { Start } from "./Start";

export function Board() {
    const layout = useAppSelector((state) => state.game.board);
    const board = layout.map((e, i) => {
        const cols = e.map((x, y) => <Shape key={y} shapeIndex={x} />);
        return (<div key={i} className="flex flex-col gap-4 justify-end">{cols}</div>);
    });

    return(
        <div className="board flex gap-4 mx-auto my-0 p-4 relative border border-slate-100">
            {layout.length < 1 ?
                <Start />
            :
                board
            }
        </div>
    )
}